let http = new XMLHttpRequest();
http.open('get', './data/data.json', true);
http.send();

http.onload = function (){
    if(this.readyState == 4 && this.status == 200){
        let objects = JSON.parse(this.responseText);
        let output = "";
        for(let item of objects){
            let date = item.date;
            let dateFormat = new Date(date);
            let year = dateFormat.getFullYear();
            let month = dateFormat.toLocaleString('default', { month: 'short' });;
            let day = dateFormat.getDate();
            let newDateFormat = day + " " + month + " " + year;
            output += `
            <div class="card" id="card-id">
                <div class="card-container">
                    <div class="navbar">
                        <div class="inline">
                            <div class="profile-pic">
                                <img src="${item.profile_image}"/>
                            </div>
                            <div class="post-cred">
                                <div class="profile-name">
                                    <p>${item.name}</p>
                                </div>
                                <div class="date-post">
                                    <p>${newDateFormat}</p>
                                </div>
                            </div>
                        </div>
                        <div class="socialnetwork-logo">
                            <img src="./images/instagram-logo.svg"/>
                        </div>
                    </div>
                    <div class="content">
                        <img class="multiple-img" src="./images/multiple-images-icon.svg"/>
                        <div onclick="fullScreen()">
                            <img class="img" src="${item.image}"/>
                        </div>
                    </div>
                    <div class="description">
                        <p>${item.caption}</p>
                    </div>
                    <hr class="divide">
                    <div class="likes">
                        <div class="heart-logo">
                                <svg class="heartButton" id="btn" onclick="toggleHeart(this)" width="17" height="17" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M14.7617 3.26543C14.3999 2.90347 13.9703 2.61634 13.4976 2.42045C13.0248 2.22455 12.518 2.12372 12.0063 2.12372C11.4945 2.12372 10.9878 2.22455 10.515 2.42045C10.0422 2.61634 9.61263 2.90347 9.25085 3.26543L8.50001 4.01626L7.74918 3.26543C7.0184 2.53465 6.02725 2.1241 4.99376 2.1241C3.96028 2.1241 2.96913 2.53465 2.23835 3.26543C1.50756 3.99621 1.09702 4.98736 1.09702 6.02084C1.09702 7.05433 1.50756 8.04548 2.23835 8.77626L2.98918 9.52709L8.50001 15.0379L14.0108 9.52709L14.7617 8.77626C15.1236 8.41448 15.4108 7.98492 15.6067 7.51214C15.8026 7.03935 15.9034 6.53261 15.9034 6.02084C15.9034 5.50908 15.8026 5.00233 15.6067 4.52955C15.4108 4.05677 15.1236 3.62721 14.7617 3.26543V3.26543Z" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                                <div class="like-counter">
                                    <p class="counter-toggle" value=${item.likes}>${item.likes}</p>
                                </div>
                        </div>
                    </div>
                </div>
            </div>`;
        }


        document.querySelector(".container").innerHTML = output;
    }
}

function toggleHeart(aTag) {

    var content = aTag.parentNode.getElementsByClassName("heartButton")[0];
    var counter = aTag.parentNode.getElementsByClassName("counter-toggle")[0];
    console.log(content);
    if(content.style.fill == null || content.style.fill == "red")
    {
        content.style.fill = 'none';
        counter.innerHTML = parseInt(counter.innerHTML)-1;
    }
    else
    {
        content.style.fill = 'red';
        counter.innerHTML = parseInt(counter.innerHTML)+1;
    }
}



var currentItem = 4;
function loadMoreFunction(){
    var cards = document.querySelectorAll(".card");
    var btn = document.querySelector("#loadBtn");
        for(var i = currentItem; i < currentItem + 4; i++ ){
            if(cards[i] ){
                cards[i].style.display = 'flex';
            }
        }
    currentItem += 4;
    var container = document.getElementById("container");
        if(currentItem == container.children.length){
            btn.style.display = 'none';
        }else{
            btn.style.display = 'flex';
        }
}


const lightboxBg = document.createElement("div");
lightboxBg.id = "lightboxBg";
document.body.appendChild(lightboxBg);
const images = document.querySelectorAll('.img');

function fullScreen(){
    lightboxBg.classList.add('active');
    const lightboxImg = document.createElement('img');
    lightboxImg.src = document.querySelector('.img').value = event.target.src;
    lightboxImg.id ="lightboxImg";
    let element = document.getElementById("lightboxBg");
    while (element.firstChild) {
      element.removeChild(element.firstChild);
    }
    lightboxBg.appendChild(lightboxImg);
    console.log(lightboxBg)
}

lightboxBg.addEventListener('click', e=>{
    lightboxBg.classList.remove('active');
})